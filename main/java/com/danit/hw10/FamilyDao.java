package myhomework10.main.java.com.danit.hw10;

import java.util.List;

public interface FamilyDao {
    public List getAllFamilies();
    public Family getFamilyByIndex(int index);
    public boolean deleteFamily(int index);
    public boolean deleteFamily(Family family);
    public void saveFamily(Family family);


}
