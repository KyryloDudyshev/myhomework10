package myhomework10.main.java.com.danit.hw10;

import java.util.ArrayList;
import java.util.HashSet;

public class Family {
    private Human mother;
    private Human father;
    private ArrayList<Human> children;
    private Pet pet;
    private HashSet<Pet> petsSet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<Human>();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public ArrayList<Human> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Human> children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public HashSet<Pet> getPetsSet() {
        return petsSet;
    }

    public void setPetsSet(HashSet<Pet> petsSet) {
        this.petsSet = petsSet;
    }

    @Override
    public String toString() {
        return "Family: mother: " + mother.getName() + " " + mother.getSurname() + ", " + "father: " + father.getName() + " " + father.getSurname() + ", children: " + children;
    }

    @Override
    protected void finalize(){
        System.out.println("Object Family deleted");
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public void addChild(Human human) {
        children.add(human);
    }

    public boolean deleteChild(int index) {
        if (children.size() == 0) {
            System.out.println("Incorrect index");
            return false;
        } else if (index > children.size() | index < 0) {
            System.out.println("Incorrect index");
            return false;
        } else {
            children.remove(index);
            return true;
        }
    }

    public void deleteChild(Human human) {
        int index = 0;
        for (Human child : children) {
            if (child.getName().equals(human.getName())) {
                index++;
            }
        }
        if (index == 0) {
            System.out.println("No child with this name in this family");
        }
        if (children.size() == 0) {
            System.out.println("There is no child in this family");
        } else {
            for (int i = 0; i < children.size(); i++) {
                if ((children.get(i).hashCode() == human.hashCode()) && (children.get(i).getName().equals(human.getName()))) {
                    children.remove(i);
                }
            }
        }
    }

    public int countFamily() {
        return 2 + children.size();
    }
}